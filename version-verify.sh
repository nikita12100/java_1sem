#! /bin/bash

set -x

echo "maven plugin version verification started..."

sed -i -E 's/<source>[.0-9]+<\/source>/<source>1.8<\/source>/g' pom.xml

sed -i -E 's/<target>[.0-9]+<\/target>/<target>1.8<\/target>/g' pom.xml

echo "verification finished"
